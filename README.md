# Ignite Feed

- Projeto desenvolvido com base no projeto do Ignite da RocketSeat

## Exec

- Instale as dependências com: `yarn` ou `npm install`
- Inicie o projeto com: `yarn dev` ou `npm run dev`
- Acesse em: http://localhost:5173

## Projeto final

<img src="./src/assets/capa.png" />
