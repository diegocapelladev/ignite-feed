import styles from '../../styles/Header.module.css'
import logo from '../../assets/ignite-logo.png'

export function Header() {
  return (
    <header className={styles.header}>
      <img src={logo} alt="" />
      <h1>Ignite Feed</h1>
    </header>
  )
}
